package org.usfirst.frc.team3501.robot.subsystems;

import org.usfirst.frc.team3501.robot.Constants;
import org.usfirst.frc.team3501.robot.commands.driving.JoystickDrive;
import com.ctre.phoenix.motorcontrol.SensorCollection;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.MecanumDrive;

public class DriveTrain extends Subsystem {

  private static DriveTrain driveTrain;
  private final MecanumDrive robotDrive;

  private final WPI_TalonSRX frontLeft, frontRight, rearLeft, rearRight;
  private final SensorCollection frontBackEncoder, leftRightEncoder;

  private static final int RESET_ENC_MAXTIMEOUT = 3;
  private ADXRS450_Gyro imu;


  private DriveTrain() {
    // MOTOR CONTROLLERS
    frontLeft = new WPI_TalonSRX(Constants.DriveTrain.FRONT_LEFT);
    frontRight = new WPI_TalonSRX(Constants.DriveTrain.FRONT_RIGHT);
    rearLeft = new WPI_TalonSRX(Constants.DriveTrain.REAR_LEFT);
    rearRight = new WPI_TalonSRX(Constants.DriveTrain.REAR_RIGHT);

    SpeedControllerGroup m_left_rear = new SpeedControllerGroup(rearLeft);
    SpeedControllerGroup m_left_front = new SpeedControllerGroup(frontLeft);
    SpeedControllerGroup m_right_rear = new SpeedControllerGroup(rearRight);
    SpeedControllerGroup m_right_front = new SpeedControllerGroup(frontRight);
    robotDrive = new MecanumDrive(m_left_front, m_left_rear, m_right_front,
        m_right_rear);

    // Encoders
    frontBackEncoder = rearLeft.getSensorCollection();
    leftRightEncoder = rearRight.getSensorCollection();

    try {
      this.imu = new ADXRS450_Gyro(Constants.DriveTrain.GYRO_PORT);
      this.imu.reset();
      this.imu.calibrate();
    } catch (NullPointerException e) {
      System.out.println("Gyro Null Pointer Exception");
      this.imu = null;
    }
  }

  /**
   * @return driveTrain if one does not already exist
   */
  public static DriveTrain getDriveTrain() {
    if (driveTrain == null) {
      driveTrain = new DriveTrain();
    }
    return driveTrain;
  }

  @Override
  protected void initDefaultCommand() {
    setDefaultCommand(new JoystickDrive());
  }


  /**
   * Mecanum Drive - takes in the ySpeed, xSpeed, Z Rotation, and Gyro Angle as parameters and enters
   * those arguments in the driveCartesian method
   *
   * @param sidewaysSpeed
   * @param frontbackSpeed
   * @param rotation
   * @param is fieldOriented or not
   */
  public void mecanumDrive(final double sidewaysSpeed,
      final double frontbackSpeed, final double rotation) {

    robotDrive.driveCartesian(-sidewaysSpeed, -frontbackSpeed, -rotation);
  }

  /**
   * Stop the robot (set motor values to 0)
   */
  public void stop() {
    mecanumDrive(0, 0, 0);
  }


  // Encoders
  public double getRightLeftEncoderDistance() {
    return leftRightEncoder.getQuadraturePosition();
  }

  public double getFrontBackEncoderDistance() {
    return frontBackEncoder.getQuadraturePosition();
  }

  public void resetEncoders() {
    frontBackEncoder.setQuadraturePosition(0, RESET_ENC_MAXTIMEOUT);
    leftRightEncoder.setQuadraturePosition(0, RESET_ENC_MAXTIMEOUT);
  }

  public double getRightLeftSpeed() {
    return leftRightEncoder.getQuadratureVelocity();
  }

  public double getFrontBackSpeed() {
    return frontBackEncoder.getQuadratureVelocity();
  }


  // Gyro
  public double getAngle() {
    if (imu != null)
      return this.imu.getAngle();
    else {
      return 0;
    }

  }

  public void resetGyro() {
    this.imu.reset();
  }

}
