package org.usfirst.frc.team3501.robot.subsystems;

import org.usfirst.frc.team3501.robot.Constants;
import org.usfirst.frc.team3501.robot.MathLib;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.SensorCollection;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Elevator extends Subsystem {

  private static Elevator elevator;

  // POSITIONS (in inches)
  public static final int BOTTOM_POS = 0;
  public static final int TOP_POS = 50;

  private final WPI_TalonSRX elevatorTalon;
  private final WPI_TalonSRX elevatorEncoderTalon;
  private final SensorCollection elevatorEncoder;

  private final DigitalInput topLimitSwitch, bottomLimitSwitch;

  public static final double SPEED = 0.30;

  private Elevator() {
    elevatorTalon = new WPI_TalonSRX(Constants.Elevator.ELEVATOR_MOTOR);
    elevatorEncoderTalon =
        new WPI_TalonSRX(Constants.Elevator.ELEVATOR_ENCODER_TALON);

    elevatorEncoder = elevatorEncoderTalon.getSensorCollection();

    topLimitSwitch = new DigitalInput(Constants.Elevator.TOP_LIMIT_SWITCH);
    bottomLimitSwitch =
        new DigitalInput(Constants.Elevator.BOTTOM_LIMIT_SWITCH);

    this.setCANTalonsBrake();
  }

  public static Elevator getElevator() {
    if (elevator == null) {
      elevator = new Elevator();
    }
    return elevator;
  }

  // MOTOR METHODS
  public void setMotorValue(double motorVal) {
    motorVal = MathLib.restrictToRange(motorVal, -1.0, 1.0);

    elevatorTalon.set(ControlMode.PercentOutput, motorVal);
  }

  public void stop() {
    setMotorValue(0);
  }

  public double getMotorVal() {
    return (elevatorTalon.getMotorOutputPercent());
  }

  public void setCANTalonsCoast() {
    elevatorTalon.setNeutralMode(NeutralMode.Coast);
  }

  public void setCANTalonsBrake() {
    elevatorTalon.setNeutralMode(NeutralMode.Brake);
  }

  // ENCODER METHODS
  public double getHeight() {
    return elevatorEncoder.getQuadraturePosition();
  }

  public double getSpeed() {
    return elevatorEncoder.getQuadratureVelocity();
  }

  public void resetEncoders() {
    elevatorEncoder.setQuadraturePosition(0, 3);
  }

  public boolean isAtTop() {
    return !topLimitSwitch.get();
  }

  public boolean isAtBottom() {
    return !bottomLimitSwitch.get();
  }

  @Override
  protected void initDefaultCommand() {}

}
