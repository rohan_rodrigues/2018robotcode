package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.Intake;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class MoveUp extends Command {
  private Intake intake = Robot.getIntake();

  public MoveUp() {
    requires(intake);
  }

  @Override
  protected void initialize() {}

  @Override
  protected void execute() {
    intake.setAngleMotorValue(-intake.upSpeed);
    System.out.println("Angle motor value: " + intake.getAngleMotorValue());
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
    intake.setAngleMotorValue(0);
  }

  @Override
  protected void interrupted() {
    System.out.println("Interrupted");
    end();
  }
}
