package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.Intake;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class MoveDown extends Command {
  private Intake intake = Robot.getIntake();

  public MoveDown() {
    requires(intake);
  }

  @Override
  protected void initialize() {}

  @Override
  protected void execute() {
    intake.setAngleMotorValue(intake.downSpeed);
    System.out.println("Angle motor value: " + intake.getAngleMotorValue());
  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
    intake.setAngleMotorValue(0);
  }

  @Override
  protected void interrupted() {
    System.out.println("Interrupted");
    end();
  }
}
