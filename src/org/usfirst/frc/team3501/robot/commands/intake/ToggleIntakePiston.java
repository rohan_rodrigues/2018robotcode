package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.subsystems.Intake;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class ToggleIntakePiston extends Command {
  private Intake intake = Intake.getIntake();

  public ToggleIntakePiston() {
    requires(intake);
  }

  @Override
  protected void initialize() {
    intake.setPistonActivated(!intake.isPistonActivated());
    System.out.println(intake.isPistonActivated());
  }

  @Override
  protected void execute() {

  }

  @Override
  protected boolean isFinished() {
    return timeSinceInitialized() > 1;
  }

  @Override
  protected void end() {
    intake.stop();
  }

  @Override
  protected void interrupted() {}
}
