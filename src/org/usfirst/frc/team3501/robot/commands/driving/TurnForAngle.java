package org.usfirst.frc.team3501.robot.commands.driving;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.DriveTrain;
import edu.wpi.first.wpilibj.command.Command;

/**
 * This command turns the robot for a specified angle in specified direction - either left or right
 */
public class TurnForAngle extends Command {
  private DriveTrain driveTrain = Robot.getDriveTrain();

  private double maxTimeOut;
  private double target;
  private double startAngle;

  /**
   * @param angle: a positive value will cause the robot to to turn right through the specified angle
   *        in degrees, and a negative value left
   * @param maxTimeOut: the max time this command will be allowed to run on for
   */
  public TurnForAngle(double angle, double maxTimeOut) {
    requires(driveTrain);
    this.startAngle = driveTrain.getAngle();
    this.maxTimeOut = maxTimeOut;
    this.target = angle;
  }

  @Override
  protected void initialize() {}

  @Override
  protected void execute() {
    this.driveTrain.mecanumDrive(0, 0, target);
  }

  @Override
  protected boolean isFinished() {
    return timeSinceInitialized() >= maxTimeOut
        || Math.abs(driveTrain.getAngle() - startAngle) >= target;
  }

  @Override
  protected void end() {}

  @Override
  protected void interrupted() {
    end();
  }
}
