package org.usfirst.frc.team3501.robot.commands.elevator;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.Elevator;
import edu.wpi.first.wpilibj.command.Command;


public class MoveToTarget extends Command {

  private Elevator elevator = Robot.getElevator();

  private double target;
  private boolean movingUp;

  /**
   * @param target the height the elevator will move to in inches
   * @param maxTimeOut the maximum time this command will be allowed to run before being cut
   */
  public MoveToTarget(double target) {
    requires(elevator);
    this.target = target;

    if (elevator.getHeight() < target) {
      movingUp = true;
    } else {
      movingUp = false;
    }
  }

  @Override
  protected void initialize() {}

  @Override
  protected void execute() {
    if (movingUp) {
      elevator.setMotorValue(elevator.SPEED);
    } else {
      elevator.setMotorValue(-elevator.SPEED);
    }

  }

  @Override
  protected boolean isFinished() {
    if (movingUp) {
      return elevator.getHeight() >= target || elevator.isAtTop();
    } else {
      return elevator.getHeight() <= target || elevator.isAtBottom();
    }
  }

  @Override
  protected void end() {
    if (elevator.isAtBottom()) {
      this.elevator.resetEncoders();
    }

    this.elevator.stop();
  }

  @Override
  protected void interrupted() {
    end();
  }
}
